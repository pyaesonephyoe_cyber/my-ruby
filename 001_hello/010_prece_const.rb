class Foo
  CONST = 'Foo'
end

class Bar
  CONST = 'Bar'
  class Baz<Foo
    p CONST # => "Bar" outer constant
    # In this case, parent class constants are not visible unless explicitly specified
    p Foo :: CONST # => "Foo"
  end
end