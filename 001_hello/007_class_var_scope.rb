class Foo
	@@a=:a
	class << Foo
		p @@a
		puts @@a
	end
	def Foo.a1
		p @@a
		puts @@a
	end
end
Foo.a1
def Foo.a2
	p @@a #NameError
	puts @@a
end
Foo.a2
class << Foo
	p @@a
	puts @@a #NameError
end