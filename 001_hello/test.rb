class Foo
  @@ a =: a
  class << Foo
    p @@ a # =>: a
  end

  def Foo.a1
    p @@ a
  end
end

Foo.a1 # =>: a

def Foo.a2
  p @@ a
end
Foo.a2 # => NameError

class << Foo
  p @@ a # => NameError.
end