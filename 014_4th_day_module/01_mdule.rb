module MathMethods
	def addition(x,y)
		return x+y
	end
	def multiply(x,y)
		return x*y
	end
	def subtract(x,y)
		return x-y
	end
	def divide(x,y)
		x=x.to_f
		if y==0
			return "Cannot divide by Zero"
		else 
			return (x/y)
		end
	end
	def power(x,y)
		return x**y
	end
end
class NumberCalc
	include MathMethods
	
	def showRes(num1,num2)
		print("#{num1} + #{num2}= #{addition(num1,num2)}\n")
		print("#{num1} - #{num2}= #{subtract(num1,num2)}\n")
		print("#{num1} * #{num2}= #{multiply(num1,num2)}\n")
		print("#{num1} / #{num2}= #{divide(num1,num2)}\n")
		print("#{num1} ** #{num2}= #{power(num1,num2)}\n")
	end

end
book=NumberCalc.new
book.showRes(1,2)


				