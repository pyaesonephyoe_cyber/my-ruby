price={"Banana"=>120,"Apple"=>130,"Orange"=>100}

print ("The price of Banana is #{price["Banana"]} \n")
print ("The price of Apple is #{price["Apple"]} \n")
print ("The price of Orange is #{price["Orange"]} \n")
print("#{price.size} fruit avalilable \n")
print("-----#{price.class}-------------\n \n")

price["Banana"]=100
price["Watermelon"]=200
print ("The price of Banana is #{price["Banana"]} \n")
print ("The price of Apple is #{price["Apple"]} \n")
print ("The price of Orange is #{price["Orange"]} \n")
print ("The price of Watermelon is #{price["Watermelon"]} \n")
print("#{price.length} fruit avalilable \n")
print("-----#{price.class}-------------\n \n")
price.store("Apple",300)
price.store("Mango",400)
print ("The price of Banana is #{price["Banana"]} \n")
print ("The price of Apple is #{price["Apple"]} \n")
print ("The price of Orange is #{price["Orange"]} \n")
print ("The price of Watermelon is #{price["Watermelon"]} \n")
print ("The price of Mango is #{price["Mango"]} \n")
print("#{price.length} fruit avalilable \n")
print("-----#{price.class}-------------\n \n")