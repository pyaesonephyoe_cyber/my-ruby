class Team
	def initialize(name)
		@teamname=name
	end
	def showName(league)
		print("#{@teamname} is a team in #{league}.\n")
	end
end
class FootballTeam < Team
	def showName(league)
		super
		print("This is belongs to #{league}.\n")
	end
end
psg=FootballTeam.new("Paris Saint Germain")
psg.showName("League One")