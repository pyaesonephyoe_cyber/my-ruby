def add(num1,num2)
	return num1+num2
end
def sub(num1,num2)
	if num1>=num2
		return num1-num2
	else
		return num2-num1
	end
end
def multiply(num1,num2)
	return num1*num2
end
def divide(num1,num2)
	if num2 ==0 
		return "Cannot divide by zero"
	else 
		return num1/num2
	end
end


def showResult(result)
	print(result,"\n")
end
showResult(add(1,2))
showResult(sub(34,50))
showResult(multiply(4,6))
showResult(divide(1,0))
print("--------------------------\n")

def allMath(num1,num2)
	print("Num1 =",num1,"\n")
	print("Num2 =",num2,"\n")
	addRes=num1+num2
	subRes=num1-num2
	multiplyRes=num1*num2
	divideRes=num1/num2
	return addRes,subRes,multiplyRes,divideRes

end
plus,minus,star,backslack=allMath(24,12)
print("Addition =",plus,"\n")
print("Subtraction =",minus,"\n")
print("Multiplication =",star,"\n")
print("Division =",backslack,"\n")

	