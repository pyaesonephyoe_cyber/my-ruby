class Country
	def initialize(countryname)
		@country=countryname
	end

	def displayClass
		print("#{@country} is a Country \n")
	end
	def displayCity(city)
		print("The city #{city} is in #{@country} .\n")
	end

end

myanmar=Country.new("Myanmar")
myanmar.displayCity("Pakokku")
myanmar.displayClass