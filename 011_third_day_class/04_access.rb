class Laptop
	def initialize(name="Pc",processor="Pendium 3")
		@pc_name=name
		@pc_processor=processor
	end
	def getName
		return @pc_name
	end
	def getProcessor
		return @pc_processor
	end

	attr_accessor :pc_name
	attr_accessor :pc_processor
end

lenovo=Laptop.new()
lenovo.pc_name="Lenovo"
lenovo.pc_processor="Core i3"
print("#{lenovo.getName} \n")
print("#{lenovo.getProcessor}\n")
