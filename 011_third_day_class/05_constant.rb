class Circle
	PI=3.142
	def initialize(radius=0)
		@r=radius
	end
	def addNewRadius(addition=0)
		@r+=addition
	end
	def calculateArea
		return @r*@r*PI
	end
end

cycle=Circle.new()
cycle.addNewRadius(7)
print(cycle.calculateArea,"\n")
print(Circle::PI)


