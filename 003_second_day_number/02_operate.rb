a=100
b=10
c=3
d=2

ab=a+b
print(ab,"\n")
ba=a-b
print(ba,"\n")
cd=c*d
print(cd,"\n")
cdd=c**d
print(cdd,"\n")
abb=a/b
print(abb,"\n")
dc=c%d
print(dc,"\n")

print("5 * 3 = ", 5 * 3, "\n")
print("8 / 2 = ", 8 / 2, "\n")
print("4 + 6 = ", 4 + 6, "\n")
print("7 - 2 = ", 7 - 2, "\n")
print("5 % 2 = ", 5 % 2, "\n")
print("5 ** 3 = ", 5 ** 3, "\n")


print("5 + 3 * 4 = ", 5 + 3 * 4, "\n")
print("5 + 3 - 4 = ", 5 + 3 - 4, "\n")
print("(5 + 3) * 4 = ", (5 + 3) * 4, "\n")
print("2 * ((5 + 3) * 4 - (4 + 5) / 3) = ", 2 * ((5 + 3) * 4 - (4 + 5) / 3), "\n")