num1 = 11
num2 = 14
print(num1, " & ", num2, " = ", num1 & num2, "\n")

num1 = 10
num2 = 12
print(num1, " | ", num2, " = ", num1 | num2, "\n")

print(num1, " ^ ", num2, " = ", num1 ^ num2, "\n")
print("~ ", num1, " = ", ~11, "\n")

num1 = 11
num2 = 1
print(num1, " << ", num2, " = ", num1 << num2, "\n")
print(num1, " >> ", num2, " = ", num1 >> num2, "\n")

num2 = 2
print(num1, " << ", num2, " = ", num1 << num2, "\n")
print(num1, " >> ", num2, " = ", num1 >> num2, "\n")

num1 = -10
num2 = 1
print(num1, " >> ", num2, " = ", num1 >> num2, "\n")