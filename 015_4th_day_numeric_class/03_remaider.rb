print("13 % 4 = ", 13 % 4, "\n")
print("13 % (-4) = ", 13 % (-4), "\n")
print("(-13) % 4 = ", (-13) % 4, "\n")
print("(-13) % (-4) = ", (-13) % (-4), "\n")

print("13.modulo(4) = ", 13.modulo(4), "\n")
print("13.modulo(-4) = ", 13.modulo(-4), "\n")
print("(-13).modulo(4) = ", (-13).modulo(4), "\n")
print("(-13).modulo(-4) = ", (-13).modulo(-4), "\n")

a=100.divmod(3)
print("100%3=#{a[1]} and 100/3=#{a[0]}.\n")