puts "Start"

color=Array["Red","Green","Blue"]
print(color[0]+"\n")
print(color[1]+"\n")
print(color[2]+"\n")

puts "After"
color[2]="Yellow"
print(color[0]+"\n")
print(color[1]+"\n")
print(color[2]+"\n")

puts "End"