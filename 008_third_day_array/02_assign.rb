arr1=Array["red","red","red"]

print("arr1=[\"red\",\"red\",\"red\"] \n")
print(arr1[0].object_id,"\n")
print(arr1[1].object_id,"\n")
print(arr1[2].object_id,"\n")

arr2=Array.new(3,"red")
print("arr2=Array.new(3,\"red\") \n")
print(arr2[0].object_id,"\n")
print(arr2[1].object_id,"\n")
print(arr2[2].object_id,"\n")

arr3=Array.new(3){"red"}
print("arr3=Array.new(3){\"red\"} \n")
print(arr3[0].object_id,"\n")
print(arr3[1].object_id,"\n")
print(arr3[2].object_id,"\n")

arr4=Array.new(arr2)
print("arr4=Array.new(arr2) \n")
print(arr4[0].object_id,"\n")
print(arr4[1].object_id,"\n")
print(arr4[2].object_id,"\n")
