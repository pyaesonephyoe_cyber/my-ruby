puts "1.upto(10)"
1.upto(10){|n1|
	print("Number One=#{n1} \n")

}

puts "10.downto(1)"
10.downto(1){|n2|
	print("Number Two=#{n2} \n")

}
puts "3.times"
3.times{|n3|
	print("Number n3=#{n3} \n")

}

puts "10.step(100,10)"
10.step(100,10){|n4|
	print("Number n4=#{n4} \n")
}
puts "100.step(10,-10)"
100.step(10,-10){|n5|
	print("Number n5=#{n5} \n")

}